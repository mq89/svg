# SVG

My collection of SVG icons.

## Download

[Download] source SVG and automatically generated PNG and PDF exports as ZIP.

## Constraints

- Icon sizes:
  + 48x48px
  + 100x100px


[Download]:https://gitlab.com/Mq_/svg/-/jobs/artifacts/master/download?job=Build+Images
